package org.nrg.scheduling.test

import org.nrg.scheduling.Job
import org.nrg.scheduling.JobList
import org.nrg.scheduling.ProcessorSchedulingProblem

class ExampleJobs {

    static final Job<String> shortJob = new Job<>('short', 100)
    static final Job<String> mediumJob = new Job<>('medium', 500)
    static final Job<String> longJob = new Job<>('long', 1000)
    static final Job<String> reallyLongJob = new Job<>('really long', 20000)
    static final Job<String> fsJob = new Job<>('FS', 840)
    static final Job<String> manualPupJob = new Job<>('Manual PUP', 120)
    static final Job<String> fsPupJob = new Job<>('FS PUP', 150)
    static final Job<String> fbirnJob = new Job<>('FBIRN', 15)
    static final Job<String> boldJob = new Job<>('BOLD', 90)
    static final Job<String> registerMrJob = new Job<>('registerMR', 10)
    static final Job<String> wmhJob = new Job<>('WMH', 20)

    static ProcessorSchedulingProblem<String> problemA() {
        new ProcessorSchedulingProblem<>(new JobList<>(fsJob, manualPupJob, fsPupJob, fbirnJob, boldJob, registerMrJob, wmhJob), 4)
    }

    static ProcessorSchedulingProblem<Integer> problemB() {
        new ProcessorSchedulingProblem<>(new JobList<>(
                new Job<>(1, 1000000), new Job<>(2, 10), new Job<>(3, 1)
        ), 3)
    }

    static ProcessorSchedulingProblem<Integer> problemC() {
        new ProcessorSchedulingProblem<>(new JobList<>(
                new Job<>(1, 100), new Job<>(2, 70), new Job<>(3, 45), new Job<>(4, 10), new Job<>(5, 75), new Job<>(6, 80), new Job<>(7, 50), new Job<>(8, 35), new Job<>(9, 20), new Job<>(10, 3), new Job<>(11, 7)
        ), 3) // exists perfectly efficient machine allocation where all 3 machines have run time 165
    }

    static ProcessorSchedulingProblem<Integer> problemD() {
        new ProcessorSchedulingProblem<>(new JobList<>(
                [new Job<>(1, 10), new Job<>(2, 15),  new Job<>(3, 15), new Job<>(4, 50), new Job<>(5, 10), new Job<>(6, 17), new Job<>(7, 23), new Job<>(8, 31), new Job<>(9, 9)] +
                        (10 .. 17).collect { new Job<>(it, 5)} +
                        (28 .. 45).collect { new Job<>(it, 2)}
        ), 4) // exists perfectly efficient machine allocation where all 4 machines have run time 64
    }

}
