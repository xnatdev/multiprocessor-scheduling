package org.nrg.scheduling

class ProcessorSchedulingProblem<X> {

    JobList<X> jobList
    int numMachines // Variable "m" from Hochbaum and Shmoys

    ProcessorSchedulingProblem(JobList<X> jobList, int numMachines) {
        this.jobList = jobList
        this.numMachines = numMachines
    }

    /**
     * This method attempts to apply the reduction attached to the repository: if one job is very expensive, to the extent that the job's run time
     * exceeds the average total runtime for an entire processor, we can just give assign that job to its own processor and then solve the subproblem with the remaining jobs and processors.
     * I'm guessing this is a theorem that someone already proved out there, but I never found it when going over papers, so I don't know who to credit.
     * After the reduction is applied (possibly several times), the method solves the problem using a standard algorithm for the problem.
     * @param algorithm Algorithm to use for solving the problem {@link org.nrg.scheduling.Algorithms}
     * @return solution [or approximation] for the problem
     */
    ProcessorSchedulingSolution<X> solve(ProcessorSchedulingAlgorithm algorithm) {
        final ProcessorSchedulingAlgorithm usedAlgorithm = algorithm ?: Algorithms.MIP
        jobList.sort()
        ProcessorSchedulingSolution<X> solution
        if (jobList.maxJobsize() >= jobList.totalJobLength()/numMachines && numMachines > 1) { // *apply reduction*
            final JobList<X> otherJobs = jobList.partialJobList(1, jobList.size())
            final ProcessorSchedulingProblem<X> subProblem = new ProcessorSchedulingProblem<>(otherJobs, numMachines - 1)
            solution = subProblem.solve(usedAlgorithm)
            solution.addSingleMachineJob(jobList[0])
        } else {
            solution = usedAlgorithm.solve(this)
        }
        solution.sortEachList()
        solution.processMeasures()
        solution
    }

}
