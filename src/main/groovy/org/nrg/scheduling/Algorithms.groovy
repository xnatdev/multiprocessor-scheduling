package org.nrg.scheduling

class Algorithms {

    public static final ProcessorSchedulingAlgorithm HOCHBAUM_SHMOYS = new HochbaumShmoysApproximation()
    public static final ProcessorSchedulingAlgorithm MIP = new MixedIntegerLinearProgrammingDelegationAlgorithm()

    @SuppressWarnings('unused')
    static ProcessorSchedulingAlgorithm hochbaumShmoys(int numIterations) {
        return new HochbaumShmoysApproximation(numIterations)
    }

}
