package org.nrg.scheduling

class HochbaumShmoysApproximation implements ProcessorSchedulingAlgorithm {

    public static final int DEFAULT_NUM_ITERATIONS = 5
    private int numIterations // Variable "k" from Hochbaum and Shmoys

    HochbaumShmoysApproximation(int numIterations) {
        this.numIterations = numIterations
    }

    HochbaumShmoysApproximation() {
        this(DEFAULT_NUM_ITERATIONS)
    }

    @Override
    <X> ProcessorSchedulingSolution<X> solve(ProcessorSchedulingProblem<X> problem) {
        final JobList<X> jobList = problem.jobList
        final int numMachines = problem.numMachines
        final double size = Math.max(jobList.totalJobLength()/numMachines, jobList.maxJobsize())

        double upper = 2 * size
        double lower = size
        numIterations.times {
            final scalingFactor = (upper + lower) / 2 // Variable "d" from the paper
            if (new BinPackingProblem<>(scalingFactor, jobList).solveBinPackingApproximation().size() > numMachines) {
                lower = scalingFactor
            } else {
                upper = scalingFactor
            }
        }
        final ProcessorSchedulingSolution<X> processorSchedulingSolution = new ProcessorSchedulingSolution<>(new BinPackingProblem<>(upper, jobList).solveBinPackingApproximation())
        processorSchedulingSolution.restoreOriginalTimes(jobList)
        processorSchedulingSolution
    }

}
