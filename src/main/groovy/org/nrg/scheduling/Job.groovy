package org.nrg.scheduling

import org.nrg.testing.MathUtils

class Job<X> implements Comparable<Job> {

    X jobId
    double jobLength
    double launchTime
    double endTime

    Job(X jobId, double jobLength) {
        setJobId(jobId)
        setJobLength(jobLength)
    }

    void setTimes(double cumulativeMachineTime) {
        launchTime = cumulativeMachineTime
        endTime = launchTime + jobLength
    }

    @Override
    int compareTo(Job comparedJob) {
        MathUtils.differenceToIntComparison(jobLength - comparedJob.getJobLength())
    }

    int startComparison(Job comparedJob) {
        MathUtils.differenceToIntComparison(launchTime - comparedJob.getLaunchTime())
    }

    int finishComparison(Job comparedJob) {
        MathUtils.differenceToIntComparison(endTime - comparedJob.getEndTime())
    }

    @Override
    boolean equals(o) {
        if (this.is(o)) {
            return true
        } else if (!(o instanceof Job)) {
            return false
        }

        Job job = o as Job

        if (Double.compare(job.jobLength, jobLength) != 0) {
            false
        } else if (jobId != job.jobId){
            false
        } else {
            true
        }
    }

    @Override
    int hashCode() {
        final long temp = jobLength != +0.0d ? Double.doubleToLongBits(jobLength) : 0L
        31 * jobId.hashCode() + (int) (temp ^ (temp >>> 32))
    }

    @Override
    String toString() {
        "Job: ${jobId}"
    }

}