package org.nrg.scheduling.test

import org.nrg.scheduling.*
import org.nrg.testing.MathUtils
import org.testng.annotations.Test

import static org.nrg.scheduling.test.ExampleJobs.*
import static org.testng.AssertJUnit.*

class HochbaumShmoysSolutionTests {

    @Test
    void solutionTestA() {
        final ProcessorSchedulingSolution<String> solution = problemA().solve(Algorithms.HOCHBAUM_SHMOYS)

        final List<JobList<String>> solutionMachineAllocation = solution.getSolutionByMachineAllocation()

        assertTrue(MathUtils.doublesEqual(solution.getMakespan(), 840))
        assertEquals(4, solutionMachineAllocation.size())
        assertEquals(new JobList(fsJob), solutionMachineAllocation[0])
        assertEquals(new JobList(fsPupJob), solutionMachineAllocation[1])
        assertEquals(
                [new JobList<>(manualPupJob, registerMrJob), new JobList<>(boldJob, wmhJob, fbirnJob)] as Set,
                solutionMachineAllocation.subList(2, 4) as Set
        )
        assertEquals(new JobList<>(boldJob, wmhJob, manualPupJob, fbirnJob, registerMrJob, fsPupJob, fsJob), solution.getSolutionByCompletionOrder())
    }

    @Test
    void solutionTestB() {
        assertTrue(MathUtils.doublesEqual(problemB().jobList[0].jobLength, problemB().solve(Algorithms.HOCHBAUM_SHMOYS).getMakespan()))
    }

    @Test
    void solutionTestC() {
        assertTheorem1(problemC().solve(Algorithms.HOCHBAUM_SHMOYS), 165)
    }

    @Test
    void solutionTestD() {
        assertTheorem1(problemD().solve(Algorithms.HOCHBAUM_SHMOYS), 90)
    }

    @SuppressWarnings('GrMethodMayBeStatic')
    private void assertTheorem1(ProcessorSchedulingSolution solution, int minimalMakespan) {
        assertTrue(solution.getMakespan() <= 1.2*(1 + Math.pow(2, -HochbaumShmoysApproximation.DEFAULT_NUM_ITERATIONS)) * minimalMakespan) // checks that theorem 1 from Hochbaum & Shmoys is satisfied
    }

}
