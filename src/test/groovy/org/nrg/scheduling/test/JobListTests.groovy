package org.nrg.scheduling.test

import org.nrg.scheduling.JobList
import org.nrg.testing.MathUtils
import org.testng.annotations.Test

import static org.testng.AssertJUnit.*

class JobListTests {

    @Test
    void testSorting() {
        final JobList<String> sorted = new JobList<>(ExampleJobs.reallyLongJob, ExampleJobs.longJob, ExampleJobs.mediumJob, ExampleJobs.shortJob)
        final JobList<String> unsorted = new JobList<>(ExampleJobs.longJob, ExampleJobs.shortJob, ExampleJobs.mediumJob, ExampleJobs.reallyLongJob)
        unsorted.sort()
        assertEquals(sorted, unsorted)
    }

    @Test
    void testSizes() {
        final JobList<String> jobList = new JobList<>(ExampleJobs.longJob, ExampleJobs.reallyLongJob, ExampleJobs.mediumJob, ExampleJobs.shortJob)
        assertTrue(MathUtils.doublesEqual(jobList.totalJobLength(), 20000 + 1000 + 500 + 100))
        assertTrue(MathUtils.doublesEqual(jobList.maxJobsize(), 20000))
    }

}
