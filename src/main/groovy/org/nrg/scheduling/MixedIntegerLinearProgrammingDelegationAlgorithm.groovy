package org.nrg.scheduling

import org.ojalgo.optimisation.Expression
import org.ojalgo.optimisation.ExpressionsBasedModel
import org.ojalgo.optimisation.Optimisation
import org.ojalgo.optimisation.Variable

class MixedIntegerLinearProgrammingDelegationAlgorithm implements ProcessorSchedulingAlgorithm {

    @Override
    <X> ProcessorSchedulingSolution<X> solve(ProcessorSchedulingProblem<X> problem) {
        final JobList<X> jobList = problem.jobList
        final List<Integer> processorIndexList = (0 ..< problem.numMachines)
        final List<Double> runLengths = jobList.collect { it.jobLength }

        final ExpressionsBasedModel model = new ExpressionsBasedModel()

        final List<List<Variable>> indicatorVariables = (0 ..< jobList.size()).collect { i ->
            processorIndexList.collect { j ->
                model.addVariable("indicator[${i},${j}]").binary()
            }
        }
        final Variable makespan = model.addVariable('Makespan').lower(0).weight(1).integer(false)

        indicatorVariables.eachWithIndex { indicatorsForOneJob, index ->
            final Expression jobAssignedToExactlyOneProcessorConstraint = model.addExpression("Job ${index} assignment constraint [exactly one processor]").lower(1).upper(1)
            indicatorsForOneJob.each { indicatorVariable ->
                jobAssignedToExactlyOneProcessorConstraint.set(indicatorVariable, 1)
            }
        }

        processorIndexList.each { processorIndex ->
            final Expression makespanWeaklyGreaterThanProcessorTime = model.addExpression("Processor ${processorIndex} constraint [makespan >= processor time]").lower(0).set(makespan, 1)
            indicatorVariables.eachWithIndex { indicatorsForOneJob, jobIndex ->
                makespanWeaklyGreaterThanProcessorTime.set(indicatorsForOneJob[processorIndex], -runLengths[jobIndex])
            }
        }

        final Optimisation.Result result = model.minimise() // makespan minimized [only variable with weight]
        if (result.state != Optimisation.State.OPTIMAL) {
            throw new AssertionError('Could not solve makespan minimization problem.')
        }

        final ProcessorSchedulingSolution<X> solution = new ProcessorSchedulingSolution<>(processorIndexList.size())
        indicatorVariables.eachWithIndex { indicatorsForOneJob, jobIndex ->
            solution.assignJob(jobList[jobIndex], indicatorsForOneJob.findIndexOf { indicator ->
                Math.round(indicator.value) == 1
            })
        }
        solution
    }

}