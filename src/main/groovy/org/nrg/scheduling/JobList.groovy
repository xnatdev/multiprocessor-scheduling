package org.nrg.scheduling

class JobList<X> extends ArrayList<Job<X>> {

    JobList(Job<X>... elements) {
        elements.each { element ->
            add(element)
        }
    }

    JobList(List<Job<X>> jobsToAdd) {
        this(jobsToAdd.toArray([] as Job[]))
    }

    double maxJobsize() {
        jobs().min().jobLength
    }

    double totalJobLength() {
        jobs().jobLength.sum() as double
    }

    JobList<X> cherryPick(int... indices) {
        final JobList<X> cherryPicked = new JobList<>(indices.collect {
            get(it)
        })
        cherryPicked.sort()
        cherryPicked
    }

    JobList<X> cherryPick(List<Integer> indices) {
        cherryPick(indices.toArray([]) as int[])
    }

    JobList<X> deepCopy() {
        partialJobList(0, size())
    }
    
    JobList<X> partialJobList(int fromIndex, int toIndex) {
        (fromIndex == toIndex) ? new JobList<>() : cherryPick(fromIndex ..< toIndex as int[])
    }

    /**
     * Scales all job times by some scaling factor, returning a deep copy of this
     * @param scalingFactor factor to rescale by
     * @return deep-copied rescaled list
     */
    JobList<X> rescaleJobs(double scalingFactor) {
        new JobList<>(
                jobs().collect { job ->
                    new Job<>(job.jobId, job.jobLength/scalingFactor)
                }
        )
    }

    private List<Job> jobs() {
        this
    }

}
