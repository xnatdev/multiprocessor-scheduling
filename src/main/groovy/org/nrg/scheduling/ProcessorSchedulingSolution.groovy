package org.nrg.scheduling

class ProcessorSchedulingSolution<X> {

    private List<JobList<X>> machineAllocations
    private final JobList<X> queueingOrder = new JobList<>()
    private final JobList<X> completionOrder = new JobList<>()

    ProcessorSchedulingSolution(int numMachines) {
        machineAllocations = (0 ..< numMachines).collect { new JobList<>() }
    }

    ProcessorSchedulingSolution(List<JobList<X>> machineAllocations) {
        this.machineAllocations = machineAllocations
    }

    void addSingleMachineJob(Job<X> job) {
        machineAllocations.add(0, new JobList(job))
        processMeasures()
    }

    void assignJob(Job<X> job, int machineNumber) {
        machineAllocations[machineNumber] << job
    }

    void restoreOriginalTimes(JobList<X> originalJobList) {
        machineAllocations.each { allocation ->
            allocation.each { job ->
                job.setJobLength(originalJobList.find { it.jobId == job.jobId }.jobLength)
            }
        }
    }

    void sortEachList() {
        machineAllocations.each { allocation ->
            allocation.sort()
        }
    }

    List<JobList<X>> getSolutionByMachineAllocation() {
        machineAllocations
    }

    @SuppressWarnings('unused')
    JobList<X> getSolutionByQueueOrder() {
        queueingOrder
    }

    JobList<X> getSolutionByCompletionOrder() {
        completionOrder
    }

    double getMakespan() {
        completionOrder.last().endTime
    }

    void processMeasures() {
        final List<JobList<X>> copy = machineAllocations.collect { allocation ->
            allocation.deepCopy()
        }
        queueingOrder.clear()
        completionOrder.clear()

        copy.each { jobList ->
            double cumulativeTime = 0
            jobList.each { job ->
                job.setTimes(cumulativeTime)
                cumulativeTime += job.jobLength
            }
            queueingOrder << jobList[0] // launch the first job in every queue first
            completionOrder << jobList[0] // this list is sorted later
            jobList.remove(0)
        }

        final JobList<X> remainingJobs = new JobList<>()
        copy.each { jobList ->
            remainingJobs.addAll(jobList)
            completionOrder.addAll(jobList)
        }

        remainingJobs.sort(new Comparator<Job<X>>() {
            @Override
            int compare(Job<X> o1, Job<X> o2) {
                return o2.startComparison(o1)
            }
        })
        queueingOrder.addAll(remainingJobs)

        completionOrder.sort(new Comparator<Job<X>>() {
            @Override
            int compare(Job<X> o1, Job<X> o2) {
                return o2.finishComparison(o1)
            }
        })
    }

}
