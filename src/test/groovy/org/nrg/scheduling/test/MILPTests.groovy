package org.nrg.scheduling.test

import org.nrg.scheduling.*
import org.nrg.testing.MathUtils
import org.testng.annotations.Test

import static org.nrg.scheduling.test.ExampleJobs.*
import static org.testng.AssertJUnit.*

class MILPTests {

    @Test
    void solutionTestA() {
        final ProcessorSchedulingSolution<String> solution = problemA().solve(Algorithms.MIP)

        final List<JobList<String>> solutionMachineAllocation = solution.getSolutionByMachineAllocation()

        assertTrue(MathUtils.doublesEqual(solution.getMakespan(), 840))
        assertEquals(4, solutionMachineAllocation.size())
        assertEquals(new JobList(fsJob), solutionMachineAllocation[0])
        assertEquals(new JobList(fsPupJob), solutionMachineAllocation[1])
        assertEquals(
                [new JobList<>(manualPupJob, registerMrJob), new JobList<>(boldJob, wmhJob, fbirnJob)] as Set,
                solutionMachineAllocation.subList(2, 4) as Set
        )
        assertEquals(new JobList<>(boldJob, wmhJob, manualPupJob, fbirnJob, registerMrJob, fsPupJob, fsJob), solution.getSolutionByCompletionOrder())
    }

    @Test
    void solutionTestB() {
        assertTrue(MathUtils.doublesEqual(problemB().jobList[0].jobLength, problemB().solve(Algorithms.MIP).getMakespan()))
    }

    @Test
    void solutionTestC() {
        final ProcessorSchedulingProblem<Integer> problem = problemC()
        assertTrue(MathUtils.doublesEqual(
                problem.solve(Algorithms.MIP).getMakespan(),
                problem.jobList.totalJobLength() / problem.numMachines)
        ) // no idle-time allocation
    }

    @Test
    void solutionTestD() {
        final ProcessorSchedulingProblem<Integer> problem = problemD()
        assertTrue(MathUtils.doublesEqual(
                problem.solve(Algorithms.MIP).getMakespan(),
                problem.jobList.totalJobLength() / problem.numMachines)
        ) // no idle-time allocation
    }

}
