package org.nrg.scheduling

@SuppressWarnings('GrMethodMayBeStatic')
class BinPackingProblem<X> {

    private JobList<X> jobList // assumes jobList are already sorted

    BinPackingProblem(double scalingFactor, JobList<X> jobList) {
        this.jobList = jobList.rescaleJobs(scalingFactor)
    }

    List<JobList<X>> solveBinPackingApproximation() {
        int indexCutoff = Collections.binarySearch(jobList, new Job<X>(null, 0.2))
        if (indexCutoff < 0) indexCutoff = -indexCutoff - 1
        final JobList<X> remainingJobs = jobList.partialJobList(0, indexCutoff)
        final JobList<X> smallJobs = jobList.partialJobList(indexCutoff, jobList.size())

        final List<JobList<X>> packedBins = []

        while (!remainingJobs.isEmpty()) {
            final Job<X> piece = remainingJobs.get(0)

            if (remainingJobs.size() == 1) {
                // if there's only 1 piece left to pack
                packedBins << remainingJobs.cherryPick(0)
                remainingJobs.remove(0)
            } else {
                final double pieceSize = piece.jobLength
                if (pieceSize >= 0.6) {
                    // Stage 1
                    final List<Integer> smallPieceSingleton = largestPieceIndices(remainingJobs, 1 - pieceSize)
                    if (smallPieceSingleton == null) {
                        packedBins << new JobList(piece) // if we can't
                    } else {
                        final int smallJobIndex = smallPieceSingleton[0]
                        packedBins << remainingJobs.cherryPick(0, smallJobIndex)
                        remainingJobs.remove(smallJobIndex)
                    }
                    remainingJobs.remove(0)
                } else if (remainingJobs.size() == 2) {
                    packedBins << remainingJobs.cherryPick(0, 1) // if there's only 2 pieces less than 0.6, pack 'em and call it a day. Can assume now that remainingJobs.size() >= 3
                    removeItemsFrom(remainingJobs, [0, 1])
                } else if (pieceSize >= 0.5 && pieceSize < 0.6) {
                    // Stage 2
                    if (remainingJobs[1].jobLength >= 0.5) {
                        // easy case in Stage 2:
                        packedBins << remainingJobs.cherryPick(0, 1)
                        removeItemsFrom(remainingJobs, [0, 1])
                    } else {
                        // guess version of Stage 2
                        final double twoSize = remainingJobs[1].jobLength
                        final List<Integer> threeBinCompanions = largestPieceIndices(remainingJobs, 0.3, 0.2)
                        final double threeSize = (threeBinCompanions == null) ? 0 : remainingJobs[threeBinCompanions[0]].jobLength + remainingJobs[threeBinCompanions[1]].jobLength
                        final List<Integer> indices = (twoSize >= threeSize) ? [0, 1] : [0, threeBinCompanions[0], threeBinCompanions[1]] // 2-bin or 3-bin
                        packedBins << remainingJobs.cherryPick(indices)
                        removeItemsFrom(remainingJobs, indices)
                    }
                } else if (pieceSize >= 0.4) { // already know there are three or more pieces, and all pieces are less than 0.5. So apply Stage 3
                    final List<Integer> threeBinIndices = largestPieceIndices(remainingJobs, 0.5, 0.4, 0.3)
                    final List<Integer> indices = (threeBinIndices == null) ? [0, 1] : threeBinIndices.subList(0, 3) // Stage 4 embedded here. If all pieces were too big for the above search to work
                    packedBins << remainingJobs.cherryPick(indices)
                    removeItemsFrom(remainingJobs, indices)
                } else { // Stage 4 is taken care of by earlier checks. Skip it here. Do stage 5
                    final double smallestPiece = remainingJobs.last().jobLength
                    final List<Integer> fourBinIndices = []
                    if (smallestPiece > 0.25) {
                        final double delta = 0.25 - smallestPiece
                        fourBinIndices.addAll(largestPieceIndices(remainingJobs, 0.25 + 3*delta, 0.25 + delta, 0.25 + delta/3, smallestPiece + 0.00000001) ?: []) // we want to get the smallest piece here too
                    }
                    if (smallestPiece > 0.25 || fourBinIndices.isEmpty()) {
                        while (!remainingJobs.isEmpty()) {
                            final int binSize = Math.min(3, remainingJobs.size())
                            final List<Integer> indices = 0 ..< binSize
                            packedBins << remainingJobs.cherryPick(indices)
                            removeItemsFrom(remainingJobs, indices)
                        }
                    } else {
                        final List<Integer> indices = fourBinIndices.subList(0, 4)
                        packedBins << remainingJobs.cherryPick(indices)
                        removeItemsFrom(remainingJobs, indices)
                    }
                }
            }
        }

        smallJobs.each { job ->
            final JobList<X> leastFilledBin = packedBins.min { bin ->
                bin.totalJobLength()
            }
            if (leastFilledBin.totalJobLength() > 1) {
                packedBins << new JobList<>(job)
            } else {
                leastFilledBin << job
            }
        }

        packedBins
    }

    List<Integer> largestPieceIndices(JobList<X> jobs, double... maxima) {
        // computes the indices in the list for L[u_k, ..., u_1]
        // returns null iff L[u_k, ..., u_1] does not exist
        // parameters are reversed order from the paper
        try {
            final List<Integer> indices = []
            maxima.each { maximum ->
                final int searchIndex = Collections.binarySearch(jobs, new Job<>(null, maximum))
                if (searchIndex >= 0) { // highly unlikely, it means we got a perfect match
                    if (indices.contains(searchIndex)) { // if we're already using this one, get the next smallest
                        indices << searchIndex + 1
                    } else {
                        indices << searchIndex
                    }
                } else {
                    final int insertionIndex = -searchIndex - 1
                    if (insertionIndex in indices || jobs.get(insertionIndex).jobLength > maximum) {
                        throw new ArrayIndexOutOfBoundsException("Couldn't calculate piece indices")
                    } else {
                        indices << insertionIndex
                    }
                }
            }
            indices
        } catch (Exception ignored) { // If at any point we hit this, the pieces did not exist
            null
        }
    }

    private void removeItemsFrom(JobList list, List<Integer> indices) {
        indices.sort(false).reverse().each { index -> // Start with highest indices so that removing items won't affect the index of other items we want to remove
            list.removeAt(index)
        }
    }

}
