package org.nrg.scheduling.test

import org.nrg.testing.MathUtils
import org.testng.annotations.Test

import static org.testng.AssertJUnit.*
import static org.nrg.scheduling.test.ExampleJobs.*

class JobTests {

    @Test
    void testTimes() {
        shortJob.setTimes(100)
        reallyLongJob.setTimes(100)

        assertTrue(MathUtils.doublesEqual(100, shortJob.launchTime))
        assertTrue(MathUtils.doublesEqual(200, shortJob.endTime))
        assertTrue(MathUtils.doublesEqual(20100, reallyLongJob.endTime))
    }

}
