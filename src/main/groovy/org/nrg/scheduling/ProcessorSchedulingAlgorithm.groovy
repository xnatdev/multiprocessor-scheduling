package org.nrg.scheduling

interface ProcessorSchedulingAlgorithm {

    def <X> ProcessorSchedulingSolution<X> solve(ProcessorSchedulingProblem<X> problem)

}
